require('dotenv').config();
var moment = require('moment');

const { readFileSync } = require("fs");
const { createServer } = require("https");
const { Server } = require("socket.io");
const { authorize } = require("@thream/socketio-jwt");

const httpsServer = createServer({
  key: readFileSync("/etc/letsencrypt/live/saltlabsystem.co.uk/privkey.pem"),
  cert: readFileSync("/etc/letsencrypt/live/saltlabsystem.co.uk/fullchain.pem"),
});

const io = new Server(httpsServer, {
  cors: {
    origin: '*'
  },
  transports: ['websocket'],
  //Might need adjusting, but we're sending screenshots through sockets so we can't reduce it too much
  //pingTimeout: 5000, //default: 20000; after pingTimeout ms without pong server treats client as disconnected 
  //pingInterval: 3000, ///default: 25000; after pingInterval + pingTimeout ms without ping client treats connection as closed
});

io.use(
  authorize({
    secret: process.env.JWT_KEY,
    onAuthentication: async decodedToken => {
      // return the object that you want to add to the user property
      // or throw an error if the token is unauthorized
      //console.log(decodedToken);
      return {testerId: decodedToken.sub};
    }
  })
);

io.on("connection", async (socket, next) => {
  //console.log(socket.handshake);
  //console.log(socket.handshake.auth);
  const roomId = 'EXAM_' + socket.user.testerId + '_' + socket.handshake.auth.patientId;

  if(io.sockets.adapter.rooms.has(roomId)) {
    const roomUsers = await io.in(roomId).fetchSockets();

    if(roomUsers.length >= 2) {
      //let roomUsers = await io.in(roomId).fetchSockets();
      //console.log(roomUsers);
      logMessage('Sent Error.UserAlreadyConnected event');
      socket.emit('Error.UserAlreadyConnected');
      socket.disconnect();
      return;
    }

    let index = 0;
    let hasAnyActiveUser = false;
    for(let i = 0; i < roomUsers.length; i++) {
      if(roomUsers[index].hasOwnProperty('handshake')) {
        hasAnyActiveUser = true;
        index = i;
        break;
      }
      roomUsers[index].disconnect();
    }

    if(hasAnyActiveUser && roomUsers[index].handshake.auth.mode === socket.handshake.auth.mode) {
      logMessage('Sent Error.UserAlreadyConnected event');
      socket.emit('Error.UserAlreadyConnected');
      socket.disconnect();
      return;
    }
  }

  if(socket.handshake.auth.mode === 1) {
    const roomNames = io.sockets.adapter.rooms.keys();
    for(const room of roomNames) {
      if(room.startsWith('EXAM_' + socket.user.testerId + '_') && room !== roomId) {
        const roomUsers = await io.in(room).fetchSockets();
        for(let i = 0; i < roomUsers.length; i++) {
          if(roomUsers[i].hasOwnProperty('handshake') && roomUsers[i].handshake.auth.mode === 1) {
            logMessage('Sent Error.CannotStartMoreThanOneSession event');
            socket.emit('Error.CannotStartMoreThanOneSession');
            socket.disconnect();
            return;
          }
        }
      }
    }
  }

  socket.join(roomId);

  socket.on('ping', startTimestamp => {
    socket.emit('pong', startTimestamp);
  });

  if(socket.handshake.auth.mode === 2) {
    socket.on('disconnecting', (reason) => {
      logMessage('Sent Patient.DisconnectedFromServer event');
      socket.to(roomId).emit('Patient.DisconnectedFromServer', reason);
    });
    socket.on('Patient.EnteredTestArea', () => {
      logMessage('Sent Patient.ReadyToStartSession event');
      socket.to(roomId).emit('Patient.ReadyToStartSession');
    });
    socket.on('Patient.ReportScreenSize', data => {
      //console.log('Patient.ReportScreenSize', data);
      logMessage('Sent Patient.ReportScreenSize event');
      socket.to(roomId).emit('Patient.ReportScreenSize', data);
    });
    socket.on('Patient.ConfirmedCurrentTest', testId => {
      logMessage('Sent Patient.ConfirmedCurrentTest event');
      socket.to(roomId).emit('Patient.ConfirmedCurrentTest', testId);
    });
    socket.on('Patient.ConfirmedStartingTest', data => {
      logMessage('Sent Patient.ConfirmedStartingTest event');
      socket.to(roomId).emit('Patient.ConfirmedStartingTest', data);
    });
    socket.on('Patient.ConfirmedAction', () => {
      logMessage('Sent Patient.ConfirmedAction event');
      socket.to(roomId).emit('Patient.ConfirmedAction');
    });
    socket.on('Patient.ConfirmedDisplayingItem', data => {
      logMessage('Sent Patient.ConfirmedDisplayingItem event');
      socket.to(roomId).emit('Patient.ConfirmedDisplayingItem', data);
    });
    socket.on('Patient.DrawingTouchStart', data => {
      logMessage('Sent Patient.DrawingTouchStart event');
      socket.to(roomId).emit('Patient.DrawingTouchStart', data);
    });
    socket.on('Patient.DrawingTouchMoved', data => {
      logMessage('Sent Patient.DrawingTouchMoved event');
      socket.to(roomId).emit('Patient.DrawingTouchMoved', data);
    });
    socket.on('Patient.DrawingTouchEnd', data => {
      logMessage('Sent Patient.DrawingTouchEnd event');
      socket.to(roomId).emit('Patient.DrawingTouchEnd', data);
    });
    socket.on('Patient.TouchedScreen', data => {
      logMessage('Sent Patient.TouchedScreen event');
      socket.to(roomId).emit('Patient.TouchedScreen', data);
    });
    socket.on('Patient.ReactionTime', data => {
      logMessage('Sent Patient.ReactionTime event');
      socket.to(roomId).emit('Patient.ReactionTime', data);
    });
    socket.on('Patient.TookScreenshot', data => {
      logMessage('Sent Patient.TookScreenshot event');
      socket.to(roomId).emit('Patient.TookScreenshot', data);
    });
    socket.on('Patient.WriteToLogs', data => {
      logMessage('Sent Patient.WriteToLogs event');
      socket.to(roomId).emit('Patient.WriteToLogs', data);
    });
    socket.on('Patient.TestResponse', data => {
      logMessage('Sent Patient.TestResponse event');
      socket.to(roomId).emit('Patient.TestResponse', data);
    });
  } else {
    socket.on('disconnecting', (reason) => {
      logMessage('Sent Tester.DisconnectedFromServer event');
      socket.to(roomId).emit('Tester.DisconnectedFromServer', reason);
    });
    socket.on('Tester.StartedSession', sessionData => {
      logMessage('Sent Tester.StartSession event');
      socket.to(roomId).emit('Patient.StartSession', sessionData);
    });
    socket.on('Tester.SelectedTest', testId => {
      logMessage('Sent Tester.SelectedTest event');
      socket.to(roomId).emit('Tester.SelectedTest', testId);
    });
    socket.on('Tester.SetCurrentTest', testId => {
      logMessage('Sent Tester.SetCurrentTest event');
      socket.to(roomId).emit('Tester.SetCurrentTest', testId);
    });
    socket.on('Tester.ShowTestTitle', () => {
      logMessage('Sent Tester.ShowTestTitle event');
      socket.to(roomId).emit('Tester.ShowTestTitle');
    });
    socket.on('Tester.ShowInfo', info => {
      logMessage('Sent Tester.ShowInfo event');
      socket.to(roomId).emit('Tester.ShowInfo', info);
    });
    socket.on('Tester.ShowItem', data => {
      logMessage('Sent Tester.ShowItem event');
      socket.to(roomId).emit('Tester.ShowItem', data);
    });
    socket.on('Tester.TakeScreenshot', data => {
      logMessage('Sent Tester.TakeScreenshot event');
      socket.to(roomId).emit('Tester.TakeScreenshot', data);
    });
    socket.on('Tester.RepeatAudioStimulus', data => {
      logMessage('Sent Tester.RepeatAudioStimulus event');
      socket.to(roomId).emit('Tester.RepeatAudioStimulus', data);
    });
    socket.on('Tester.StopAndUploadAudio', () => {
      logMessage('Sent Tester.StopAndUploadAudio event');
      socket.to(roomId).emit('Tester.StopAndUploadAudio');
    });
    socket.on('Tester.QuitSession', () => {
      logMessage('Sent Tester.QuitSession event');
      socket.to(roomId).emit('Tester.QuitSession');
    });
    socket.on('Tester.NewSession', data => {
      logMessage('Sent Tester.NewSession event');
      socket.to(roomId).emit('Tester.NewSession', data);
    });
  }
});

httpsServer.listen(8080);

function logMessage(msg) {
  console.log('['+ moment().format('YYYY-MM-DD HH:mm:ss+SSS') +'] ' + msg);
}